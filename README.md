# Minecraft Scripts

This is a collection of the scripts that I use to run my Minecraft server.

## Assumptions

- You already know the basics of running a Minecraft server
- You are using a Linux environment
- The server directory (containing the jar) is called `my-server`

## Dependencies

- Cron or Cronie
- [MCRCON](https://github.com/Tiiffi/mcrcon)
- Minecraft's dependencies (usually just the latest OpenJDK)
- [Verifier](https://gitlab.com/BitiTiger/verifier)

## Installation

1. Clone this repo: `git clone git@gitlab.com:BitiTiger/minecraft-scripts.git`
2. Create a `minecraft` user
3. Copy files according to the [directory map](#Directory-Mapping)
4. Install and configure cron (or cronie) according to `crontab.txt`
5. Create a symbolic link from `/home/minecraft/minecraft.service` to
  `/etc/systemd/system/minecraft.service`: `ln -s
  /home/minecraft/minecraft.service /etc/systemd/system/minecraft.service`

## Directory Mapping

| File Name         | Installed Path                                       |
| :---------------- | :--------------------------------------------------- |
| backup            | /root/backup                                         |
| rcon              | /root/rcon                                           |
| minecraft.service | /home/minecraft/minecraft.service                    |
| start             | /home/minecraft/server-files/my-server/start         |
| start_service     | /home/minecraft/server-files/my-server/start_service |

