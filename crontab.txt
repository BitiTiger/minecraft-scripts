# +--------------- Minute (0-59)
# |  +------------ Hour (0-23)
# |  |  +--------- Day of the month (1-31)
# |  |  |  +------ Month of the year (1-12)
# |  |  |  |  +--- Day of the week (0-6)
# |  |  |  |  |
 55 23  *  *  * /root/rcon "tellraw @a [{\"text\":\"Server will restart in \",\"color\":\"yellow\"},{\"text\":\"5\",\"color\":\"aqua\"},{\"text\":\" minutes.\",\"color\":\"yellow\"}]"
 56 23  *  *  * /root/rcon "tellraw @a [{\"text\":\"Server will restart in \",\"color\":\"yellow\"},{\"text\":\"4\",\"color\":\"aqua\"},{\"text\":\" minutes.\",\"color\":\"yellow\"}]"
 57 23  *  *  * /root/rcon "tellraw @a [{\"text\":\"Server will restart in \",\"color\":\"yellow\"},{\"text\":\"3\",\"color\":\"aqua\"},{\"text\":\" minutes.\",\"color\":\"yellow\"}]"
 58 23  *  *  * /root/rcon "tellraw @a [{\"text\":\"Server will restart in \",\"color\":\"yellow\"},{\"text\":\"2\",\"color\":\"aqua\"},{\"text\":\" minutes.\",\"color\":\"yellow\"}]"
 59 23  *  *  * /root/rcon "tellraw @a [{\"text\":\"Server will restart in \",\"color\":\"yellow\"},{\"text\":\"1\",\"color\":\"aqua\"},{\"text\":\" minute.\",\"color\":\"yellow\"}]"
  0  0  *  *  * /root/backup | tee -a /root/autobackup.log
